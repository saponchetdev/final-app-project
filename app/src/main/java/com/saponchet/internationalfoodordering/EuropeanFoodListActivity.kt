package com.saponchet.internationalfoodordering

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.saponchet.internationalfoodordering.adapter.ChineseFoodAdapter
import com.saponchet.internationalfoodordering.const.Layout
import com.saponchet.internationalfoodordering.databinding.EuopeanFoodListBinding

class EuropeanFoodListActivity : AppCompatActivity() {

    private lateinit var binding: EuopeanFoodListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = EuopeanFoodListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.gridRecyclerView.adapter = ChineseFoodAdapter(
            applicationContext,
            Layout.GRID
        )
        binding.gridRecyclerView.setHasFixedSize(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}