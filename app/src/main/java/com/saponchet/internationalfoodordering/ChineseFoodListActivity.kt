package com.saponchet.internationalfoodordering

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.saponchet.internationalfoodordering.adapter.ChineseFoodAdapter
import com.saponchet.internationalfoodordering.const.Layout
import com.saponchet.internationalfoodordering.databinding.ChineseFoodListBinding

class ChineseFoodListActivity : AppCompatActivity() {

    private lateinit var binding: ChineseFoodListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ChineseFoodListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.gridRecyclerView.adapter = ChineseFoodAdapter(
            applicationContext,
            Layout.GRID
        )

        // Specify fixed size to improve performance
        binding.gridRecyclerView.setHasFixedSize(true)

        // Enable up button for backward navigation
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}