package com.saponchet.internationalfoodordering.models

import androidx.annotation.DrawableRes

/**
 * A data class to represent the information presented in the dog card
 */
data class Order(
    @DrawableRes val imageResourceId: Int,
    val menu: String,
    val price: Double
)