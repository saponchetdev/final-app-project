package com.saponchet.internationalfoodordering.models

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.saponchet.internationalfoodordering.adapter.ChineseFoodAdapter
import com.saponchet.internationalfoodordering.const.Layout
import com.saponchet.internationalfoodordering.databinding.KoreanFoodListBinding

class KoreanFoodListActivity : AppCompatActivity(){
    private lateinit var binding: KoreanFoodListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = KoreanFoodListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.gridRecyclerView.adapter = ChineseFoodAdapter(
            applicationContext,
            Layout.GRID
        )
        binding.gridRecyclerView.setHasFixedSize(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

}