package com.saponchet.internationalfoodordering.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.saponchet.internationalfoodordering.R
import com.saponchet.internationalfoodordering.data.DataSource
import com.saponchet.internationalfoodordering.data.DataSource.ThaiFood

class ThaiFoodAdapter(
    private val context: Context,
    private val layout: Int
): RecyclerView.Adapter<ThaiFoodAdapter.ThaiFoodViewHolder>() {

    class ThaiFoodViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val ThaiFoodImage = view.findViewById<ImageButton>(R.id.btnThaiFood)
        val ThaiFoodMenu = view.findViewById<TextView>(R.id.thai_food_menu)
        val ThaiFoodPrice = view.findViewById<TextView>(R.id.thai_food_price)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThaiFoodViewHolder {
        val adapterLayout = when(layout){
            com.saponchet.internationalfoodordering.const.Layout.GRID -> LayoutInflater.from(parent.context)
                .inflate(R.layout.thai_food_list, parent, false)
//            com.saponchet.internationalfoodordering.const.Layout.GRID -> LayoutInflater.from(parent.context)
//                .inflate(R.layout.korean_food_list, parent, false)
//            com.saponchet.internationalfoodordering.const.Layout.GRID -> LayoutInflater.from(parent.context)
//                .inflate(R.layout.euopean_food_list, parent, false)
            else -> LayoutInflater.from(parent.context)
                .inflate(R.layout.chinese_food_list, parent, false)
        }
        return ThaiFoodViewHolder(adapterLayout)
    }

    override fun getItemCount(): Int = DataSource.ThaiFood.size // TODO: return the size of the data set instead of 0

    override fun onBindViewHolder(holder: ThaiFoodViewHolder, position: Int) {

        val ThaiFood = DataSource.ThaiFood[position]
        holder.ThaiFoodImage.setImageResource((ThaiFood.imageResourceId))
        holder.ThaiFoodMenu.text = ThaiFood.menu
        holder.ThaiFoodPrice.text = ThaiFood.price.toString()

    }
}
