package com.saponchet.internationalfoodordering.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.saponchet.internationalfoodordering.R
import com.saponchet.internationalfoodordering.data.DataSource

class ChineseFoodAdapter(
    private val context: Context,
    private val layout: Int
): RecyclerView.Adapter<ChineseFoodAdapter.ChineseFoodViewHolder>() {

    class ChineseFoodViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val ChineseFoodImage = view.findViewById<ImageButton>(R.id.btnChineseFood)
        val ChineseFoodMenu = view.findViewById<TextView>(R.id.chinese_food_menu)
        val ChineseFoodPrice = view.findViewById<TextView>(R.id.chinese_food_price)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChineseFoodViewHolder {
        val adapterLayout = when(layout){
            com.saponchet.internationalfoodordering.const.Layout.GRID -> LayoutInflater.from(parent.context)
                .inflate(R.layout.chinese_food_list, parent, false)
//            com.saponchet.internationalfoodordering.const.Layout.GRID -> LayoutInflater.from(parent.context)
//                .inflate(R.layout.korean_food_list, parent, false)
//            com.saponchet.internationalfoodordering.const.Layout.GRID -> LayoutInflater.from(parent.context)
//                .inflate(R.layout.euopean_food_list, parent, false)
            else -> LayoutInflater.from(parent.context)
                .inflate(R.layout.thai_food_list, parent, false)
        }
        return ChineseFoodViewHolder(adapterLayout)
    }

    override fun getItemCount(): Int = DataSource.ChineseFood.size // TODO: return the size of the data set instead of 0

    override fun onBindViewHolder(holder: ChineseFoodViewHolder, position: Int) {

        val ChineseFood = DataSource.ChineseFood[position]
        holder.ChineseFoodImage.setImageResource((ChineseFood.imageResourceId))
        holder.ChineseFoodMenu.text = ChineseFood.menu
        holder.ChineseFoodPrice.text = ChineseFood.price.toString()

    }
}
