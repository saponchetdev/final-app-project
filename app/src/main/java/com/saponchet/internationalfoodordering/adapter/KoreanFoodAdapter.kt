package com.saponchet.internationalfoodordering.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.saponchet.internationalfoodordering.R
import com.saponchet.internationalfoodordering.data.DataSource

class KoreanFoodAdapter (
    private val context: Context,
    private val layout: Int
): RecyclerView.Adapter<KoreanFoodAdapter.KoreanFoodViewHolder>() {

    class KoreanFoodViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val KoreanFoodImage = view.findViewById<ImageButton>(R.id.btnKoreanFood)
        val KoreanFoodMenu = view.findViewById<TextView>(R.id.european_food_menu)
        val KoreanFoodPrice = view.findViewById<TextView>(R.id.european_food_price)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KoreanFoodViewHolder {
        val adapterLayout = when(layout){
            com.saponchet.internationalfoodordering.const.Layout.GRID -> LayoutInflater.from(parent.context)
                .inflate(R.layout.korean_food_list, parent, false)
//            com.saponchet.internationalfoodordering.const.Layout.GRID -> LayoutInflater.from(parent.context)
//                .inflate(R.layout.chinese_food_list, parent, false)
//            com.saponchet.internationalfoodordering.const.Layout.GRID -> LayoutInflater.from(parent.context)
//                .inflate(R.layout.euopean_food_list, parent, false)
            else -> LayoutInflater.from(parent.context)
                .inflate(R.layout.thai_food_list, parent, false)
        }
        return KoreanFoodAdapter.KoreanFoodViewHolder(adapterLayout)
    }

    override fun getItemCount(): Int =
        DataSource.KoreanFood.size // TODO: return the size of the data set instead of 0

    override fun onBindViewHolder(holder: KoreanFoodViewHolder, position: Int) {

        val KoreanFood = DataSource.KoreanFood[position]
        holder.KoreanFoodImage.setImageResource((KoreanFood.imageResourceId))
        holder.KoreanFoodMenu.text = KoreanFood.menu
        holder.KoreanFoodPrice.text = KoreanFood.price.toString()

    }
}