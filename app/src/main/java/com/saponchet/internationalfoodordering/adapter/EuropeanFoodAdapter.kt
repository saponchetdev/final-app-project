package com.saponchet.internationalfoodordering.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.saponchet.internationalfoodordering.R
import com.saponchet.internationalfoodordering.data.DataSource

class EuropeanFoodAdapter (
    private val context: Context,
    private val layout: Int
    ): RecyclerView.Adapter<EuropeanFoodAdapter.EuropeanFoodViewHolder>() {

    class EuropeanFoodViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val EuroeanFoodImage = view.findViewById<ImageButton>(R.id.btnEuropeanFood)
        val EuroeanFoodMenu = view.findViewById<TextView>(R.id.european_food_menu)
        val EuroeanFoodPrice = view.findViewById<TextView>(R.id.european_food_price)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EuropeanFoodViewHolder {
        val adapterLayout = when(layout){
            com.saponchet.internationalfoodordering.const.Layout.GRID -> LayoutInflater.from(parent.context)
                .inflate(R.layout.euopean_food_list, parent, false)
//            com.saponchet.internationalfoodordering.const.Layout.GRID -> LayoutInflater.from(parent.context)
//                .inflate(R.layout.korean_food_list, parent, false)
//            com.saponchet.internationalfoodordering.const.Layout.GRID -> LayoutInflater.from(parent.context)
//                .inflate(R.layout.chinese_food_list, parent, false)
            else -> LayoutInflater.from(parent.context)
                .inflate(R.layout.thai_food_list, parent, false)
        }
        return EuropeanFoodAdapter.EuropeanFoodViewHolder(adapterLayout)
    }

    override fun getItemCount(): Int =
        DataSource.EuropeanFood.size // TODO: return the size of the data set instead of 0

    override fun onBindViewHolder(holder: EuropeanFoodViewHolder, position: Int) {

        val EuroeanFood = DataSource.EuropeanFood[position]
        holder.EuroeanFoodImage.setImageResource((EuroeanFood.imageResourceId))
        holder.EuroeanFoodMenu.text = EuroeanFood.menu
        holder.EuroeanFoodPrice.text = EuroeanFood.price.toString()

    }
}