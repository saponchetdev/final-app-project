package com.saponchet.internationalfoodordering.data

import com.saponchet.internationalfoodordering.R
import com.saponchet.internationalfoodordering.models.Order

object DataSource {

    val ChineseFood: List<Order> = listOf(
        Order(
            R.drawable.chinese_kungpaochiken,
            "Kung Pao Chicken",
            4.25
        ),
        Order(
            R.drawable.chinese_mapotofu,
            "Ma-Po To-Fu",
            2.85
        ),
        Order(
            R.drawable.chinese_pekingduck,
            "Peking Duck",
            4.05
        ),
        Order(
            R.drawable.chinese_steamdumpling,
            "Steam Dumpling",
            2.99
        ),
        Order(
            R.drawable.chinese_steamed_bbqporkbuns,
            "Steam Pork Buns",
            3.95
        )
    )

    val KoreanFood: List<Order> = listOf(
        Order(
            R.drawable.korean_bibimbap,
            "Bibimbap",
            3.35
        ),
        Order(
            R.drawable.korean_corndog,
            "Corn Dog",
            2.15
        ),
        Order(
            R.drawable.korean_jajangmyeon,
            "Jajangmyeon",
            3.10
        ),
        Order(
            R.drawable.korean_tteokbokki,
            "Tteokbokki",
            2.55
        ),
    )

    val EuropeanFood: List<Order> = listOf(
        Order(
            R.drawable.european_beefburger,
            "Beef Burger",
            5.15
        ),
        Order(
            R.drawable.european_beefsteak,
            "Beef Steak",
            6.89
        ),
        Order(
            R.drawable.european_pizza,
            "Pizza",
            4.55
        ),
        Order(
            R.drawable.european_salad,
            "Salad",
            2.40
        ),
        Order(
            R.drawable.european_pancake,
            "Salad",
            2.25
        ),
    )

    val ThaiFood: List<Order> = listOf(
        Order(
            R.drawable.thai_padthai,
            "Pad Thai",
            2.08
        ),
        Order(
            R.drawable.thai_tomyumkung,
            "Tom Yum Kung",
            4.45
        ),
        Order(
            R.drawable.thai_papayasalad,
            "Papaya Salad",
            1.95
        ),
        Order(
            R.drawable.thai_mangostickyrice,
            "Pad Thai",
            2.95
        ),
    )
}